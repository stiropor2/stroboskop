# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://stiropor2@bitbucket.org/stiropor2/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/stiropor2/stroboskop/commits/5335f44cae2fa8534f4831319b4712b858f2a1db

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/stiropor2/stroboskop/commits/952cb0f79676b7031f0fc06a50f58bf9b6a71ebb

Naloga 6.3.2:
https://bitbucket.org/stiropor2/stroboskop/commits/b67a2137536a51464dfafaea4237b625ac360c40

Naloga 6.3.3:
https://bitbucket.org/stiropor2/stroboskop/commits/80a3a37421a25e88999bd674c069c220e72d7e3c

Naloga 6.3.4:
https://bitbucket.org/stiropor2/stroboskop/commits/5d531732cc412fef09d90bb3241032255458b251

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/stiropor2/stroboskop/commits/396f65a129e090d27445def521a3eb0da75fea66

Naloga 6.4.2:
https://bitbucket.org/stiropor2/stroboskop/commits/dc78a743ee74e01792ffeddd278a33a926f92333

Naloga 6.4.3:
https://bitbucket.org/stiropor2/stroboskop/commits/29f166f4cd3d4174f21d289d498bc57faf3afef7

Naloga 6.4.4:
https://bitbucket.org/stiropor2/stroboskop/commits/69192fd16b912e28f0bc92121e737ce42c346d49